# OpenML dataset: StocksData

https://www.openml.org/d/43591

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The datasets talk about Stock Market,  in two Industry and each Industry has ten different companies with six columns and 600 rows  ,in the same date for all the 20 companies the period from 
2020-02-06 to 2020-03-18 one month 30 day .
Columns means :
Date column : the perioid 2020-02-06 to 2020-03-18
Open column : the open price for stock each date
Close column : the close price for stock each date
Change column:  the change between curent close price and the previous close price 
Volume Traded : total quantity of shares
Trading name : the name for the company

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43591) of an [OpenML dataset](https://www.openml.org/d/43591). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43591/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43591/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43591/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

